package org.lop.modules.repository.mybatis.generator.plugins;

public class MyIntrospectedColumnImpl extends org.mybatis.generator.api.IntrospectedColumn{

	/**
	 * 不生成withblobs
	 */
	@Override
	public boolean isBLOBColumn() {
		return false;
	}
}
